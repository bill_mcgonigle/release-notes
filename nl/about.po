# Dutch translation of the Debian release notes.
# Copyright (C) 2011, 2012 The Debian Project.
# This file is distributed under the same license as the Debian release notes.
# Eric Spreen <eric-debian@ericspreen.nl>, 2011.
# Jeroen Schot <schot@a-eskwadraat.nl>, 2012.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2017, 2019, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes/about\n"
"POT-Creation-Date: 2021-04-13 00:01+0200\n"
"PO-Revision-Date: 2021-04-13 14:19+0200\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.2.1\n"

#. type: Attribute 'lang' of: <chapter>
#: en/about.dbk:8
msgid "en"
msgstr "nl"

#. type: Content of: <chapter><title>
#: en/about.dbk:9
msgid "Introduction"
msgstr "Inleiding"

#. type: Content of: <chapter><para>
#: en/about.dbk:11
msgid ""
"This document informs users of the &debian; distribution about major changes "
"in version &release; (codenamed &releasename;)."
msgstr ""
"Dit document informeert gebruikers van de &debian;-distributie over grote "
"veranderingen in versie &release; (codenaam &releasename;)."

#. type: Content of: <chapter><para>
#: en/about.dbk:15
msgid ""
"The release notes provide information on how to upgrade safely from release "
"&oldrelease; (codenamed &oldreleasename;) to the current release and inform "
"users of known potential issues they could encounter in that process."
msgstr ""
"De notities bij de release geven informatie over hoe u veilig kunt "
"opwaarderen vanaf uitgave &oldrelease; (codenaam &oldreleasename;) naar de "
"huidige uitgave en informeren gebruikers over mogelijke moeilijkheden die ze "
"kunnen ondervinden tijdens dat proces."

#. type: Content of: <chapter><para>
#: en/about.dbk:21
msgid ""
"You can get the most recent version of this document from <ulink url=\"&url-"
"release-notes;\"></ulink>."
msgstr ""
"U kunt de meest recente versie van dit document verkrijgen vanaf <ulink url="
"\"&url-release-notes;\"></ulink>."

#. type: Content of: <chapter><caution><para>
#: en/about.dbk:26
msgid ""
"Note that it is impossible to list every known issue and that therefore a "
"selection has been made based on a combination of the expected prevalence "
"and impact of issues."
msgstr ""
"Het is helaas onmogelijk om elk bekend aandachtspunt hier te vermelden: "
"daarom is een selectie gemaakt, gebaseerd op een combinatie van de verwachte "
"frequentie van voorkomen en de ernst ervan."

#. type: Content of: <chapter><para>
#: en/about.dbk:32
msgid ""
"Please note that we only support and document upgrading from the previous "
"release of Debian (in this case, the upgrade from &oldreleasename;).  If you "
"need to upgrade from older releases, we suggest you read previous editions "
"of the release notes and upgrade to &oldreleasename; first."
msgstr ""
"Merk op dat wij enkel het opwaarderen vanaf de vorige uitgave van Debian (in "
"dit geval, opwaardering vanaf &oldreleasename;) ondersteunen en "
"documenteren. Als u vanaf oudere uitgaven moet opwaarderen, raden wij u aan "
"vorige edities van de notities bij de release te lezen en eerst op te "
"waarderen naar &oldreleasename;."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:40
msgid "Reporting bugs on this document"
msgstr "Rapporteren van fouten in dit document"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:42
msgid ""
"We have attempted to test all the different upgrade steps described in this "
"document and to anticipate all the possible issues our users might encounter."
msgstr ""
"Wij hebben getracht alle verschillende stappen in de opwaardering die in dit "
"document beschreven staan, te testen en te anticiperen op alle mogelijke "
"problemen die onze gebruikers zouden kunnen ondervinden."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:47
msgid ""
"Nevertheless, if you think you have found a bug (incorrect information or "
"information that is missing)  in this documentation, please file a bug in "
"the <ulink url=\"&url-bts;\">bug tracking system</ulink> against the "
"<systemitem role=\"package\">release-notes</systemitem> package. You might "
"first want to review the <ulink url=\"&url-bts-rn;\">existing bug reports</"
"ulink> in case the issue you've found has already been reported. Feel free "
"to add additional information to existing bug reports if you can contribute "
"content for this document."
msgstr ""
"Denkt u dat u desondanks toch nog een probleem (foutieve of ontbrekende "
"informatie) heeft gevonden in deze documentatie, dien dat dan alstublieft in "
"bij het <ulink url=\"&url-bts;\">bugopvolgingssysteem</ulink> tegen het "
"pakket <systemitem role=\"package\">release-notes</systemitem>. Het wordt "
"aangeraden eerst <ulink url=\"&url-bts-rn;\">bestaande bugrapporten</ulink> "
"na te kijken, voor het geval het probleem dat u heeft gevonden, al werd "
"gemeld. Aarzel niet om extra informatie aan bestaande bugrapporten toe te "
"voegen, mocht u inhoudelijk kunnen bijdragen aan de inhoud van dit document."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:59
msgid ""
"We appreciate, and encourage, reports providing patches to the document's "
"sources. You will find more information describing how to obtain the sources "
"of this document in <xref linkend=\"sources\"/>."
msgstr ""
"Wij waarderen en moedigen rapporten aan die verbeteringen aandragen voor de "
"broncode van dit document. Meer informatie over het verkrijgen van de "
"broncode van dit document kunt u vinden in <xref linkend=\"sources\"/>."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:67
msgid "Contributing upgrade reports"
msgstr "Bijdragen door het indienen van opwaarderingsrapporten"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:69
msgid ""
"We welcome any information from users related to upgrades from "
"&oldreleasename; to &releasename;.  If you are willing to share information "
"please file a bug in the <ulink url=\"&url-bts;\">bug tracking system</"
"ulink> against the <systemitem role=\"package\">upgrade-reports</systemitem> "
"package with your results.  We request that you compress any attachments "
"that are included (using <command>gzip</command>)."
msgstr ""
"Wij waarderen alle informatie van gebruikers, die gerelateerd is aan "
"opwaarderingen vanaf &oldreleasename; naar &releasename;. Indien u bereid "
"bent informatie te delen, dien dan alstublieft een bugrapport met uw "
"resultaten in bij het <ulink url=\"&url-bts;\">bugopvolgingssysteem</ulink> "
"tegen het pakket <systemitem role=\"package\">upgrade-reports</systemitem>. "
"We verzoeken u om alle bijlagen te comprimeren (met behulp van "
"<command>gzip</command>)."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:78
msgid ""
"Please include the following information when submitting your upgrade report:"
msgstr ""
"Voeg alstublieft de volgende informatie toe, wanneer u uw "
"opwaarderingsrapport indient:"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:85
msgid ""
"The status of your package database before and after the upgrade: "
"<systemitem role=\"package\">dpkg</systemitem>'s status database available "
"at <filename>/var/lib/dpkg/status</filename> and <systemitem role=\"package"
"\">apt</systemitem>'s package state information, available at <filename>/var/"
"lib/apt/extended_states</filename>.  You should have made a backup before "
"the upgrade as described at <xref linkend=\"data-backup\"/>, but you can "
"also find backups of <filename>/var/lib/dpkg/status</filename> in <filename>/"
"var/backups</filename>."
msgstr ""
"De status van uw pakketdatabase voor en na de opwaardering: de "
"statusdatabase van <systemitem role=\"package\">dpkg</systemitem>, "
"beschikbaar onder <filename>/var/lib/dpkg/status</filename> en de "
"toestandsinformatie van <systemitem role=\"package\">apt</systemitem>, "
"beschikbaar onder <filename>/var/lib/apt/extended_states</filename>. Het is "
"aan te raden voor het opwaarderen een reservekopie te maken, zoals "
"beschreven in <xref linkend=\"data-backup\"/>, maar u kunt ook "
"reservekopieën van <filename>/var/lib/dpkg/status</filename> vinden in "
"<filename>/var/backups</filename>."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:98
msgid ""
"Session logs created using <command>script</command>, as described in <xref "
"linkend=\"record-session\"/>."
msgstr ""
"Sessielogboeken die gemaakt zijn met <command>script</command>, zoals "
"beschreven in <xref linkend=\"record-session\"/>."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:104
msgid ""
"Your <systemitem role=\"package\">apt</systemitem> logs, available at "
"<filename>/var/log/apt/term.log</filename>, or your <command>aptitude</"
"command> logs, available at <filename>/var/log/aptitude</filename>."
msgstr ""
"Uw <systemitem role=\"package\">apt</systemitem>-logboeken, beschikbaar "
"onder <filename>/var/log/apt/term.log</filename>, of uw <command>aptitude</"
"command>-logboeken, beschikbaar onder <filename>/var/log/aptitude</filename>."

#. type: Content of: <chapter><section><note><para>
#: en/about.dbk:113
msgid ""
"You should take some time to review and remove any sensitive and/or "
"confidential information from the logs before including them in a bug report "
"as the information will be published in a public database."
msgstr ""
"U dient de tijd te nemen om alle gevoelige en/of vertrouwelijke informatie "
"in de logboeken na te kijken en te verwijderen, alvorens ze te publiceren "
"als bugrapport. Dit omdat de informatie in een publieke database wordt "
"gepubliceerd."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:122
msgid "Sources for this document"
msgstr "Broncode voor dit document"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:124
msgid ""
"The source of this document is in DocBook XML<indexterm><primary>DocBook "
"XML</primary></indexterm> format. The HTML version is generated using "
"<systemitem role=\"package\">docbook-xsl</systemitem> and <systemitem role="
"\"package\">xsltproc</systemitem>. The PDF version is generated using "
"<systemitem role=\"package\">dblatex</systemitem> or <systemitem role="
"\"package\">xmlroff</systemitem>. Sources for the Release Notes are "
"available in the Git repository of the <emphasis>Debian Documentation "
"Project</emphasis>.  You can use the <ulink url=\"&url-vcs-release-notes;"
"\">web interface</ulink> to access its files individually through the web "
"and see their changes.  For more information on how to access Git please "
"consult the <ulink url=\"&url-ddp-vcs-info;\">Debian Documentation Project "
"VCS information pages</ulink>."
msgstr ""
"De broncode van dit document is opgemaakt in het formaat Docbook "
"XML<indexterm><primary>DocBook XML</primary></indexterm>. De HTML-versie "
"wordt gegenereerd met behulp van <systemitem role=\"package\">docbook-xsl</"
"systemitem> en <systemitem role=\"package\">xsltproc</systemitem>. De PDF-"
"versie wordt gegenereerd met behulp van <systemitem role=\"package"
"\">dblatex</systemitem> of <systemitem role=\"package\">xmlroff</"
"systemitem>. De broncode voor de notities bij de release is beschikbaar in "
"het Git-archief van het <emphasis>Debian Documentatieproject</emphasis>. U "
"kunt de <ulink url=\"&url-vcs-release-notes;\">web-interface</ulink> "
"gebruiken om via het web individuele bestanden te raadplegen en "
"veranderingen eraan te bekijken. Meer informatie over toegang tot het Git-"
"archief kunt u vinden op de <ulink url=\"&url-ddp-vcs-info;\">VCS-"
"informatiepagina's van het Debian Documentatieproject</ulink>."

#~ msgid "TODO: any more things to add here?\n"
#~ msgstr "TEDOEN: nog meer hier toe te voegen?\n"
